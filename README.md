# Project Blaze android_vendor_gms

## Basic usage

- If you just wanna use this repository, then you just need to include `products/gms.mk`

```bash
$(call inherit-product, vendor/gms/products/gms.mk)
```

### After Clone Pull Lfs Files by 

Go into gms directory

```bash
git lfs fetch --all
```

or you can pull lfs files with source sync command

```bash
repo sync -c -j$(nproc --all) --force-sync --optimized-fetch --no-tags --no-clone-bundle --prune && repo forall -c git lfs pull
```

### Per Device Customization

You can choose which app you would like to exclude from GMS

Below are few apps you can exclude by adding these flags to your `blaze_(devicecodename).mk`


- Google Chrome & WebViewGoogle

```bash
TARGET_INCLUDES_GOOGLE_CHROME := false
```

- Google Tag

```bash
TARGET_INCLUDES_TAG_GOOGLE := false
```

- Google Maps

```bash
TARGET_INCLUDES_GOOGLE_MAPS := false
```

- Google Photos

```bash
TARGET_INCLUDES_GOOGLE_PHOTOS := false
```

- Google Gmail

```bash
TARGET_INCLUDES_GMAIL := false
```

- Google Sound Amplifier

```bash
TARGET_INCLUDES_SOUND_AMPLIFIER := false
```

- Google ARCore

```bash
TARGET_INCLUDES_ARCORE := false
```

- Google Talkback

```bash
TARGET_INCLUDES_TALKBACK := false
```

- Google Live Wallpaper

```bash
TARGET_INCLUDES_LIVE_WALLPAPER := false
```

- Google Pixel Wallpaper, Wallpaper Effect, Emoji Wallpaper

```bash
TARGET_INCLUDES_PIXEL_WALLPAPER := false
```

- Google AI Wallpaper

```bash
TARGET_INCLUDES_AI_WALLPAPER := false
```

- Google Recorder

```bash
TARGET_INCLUDES_GOOGLE_RECORDER := false
```

- Google Wellbeing

```bash
TARGET_INCLUDES_GOOGLE_WELLBEING := false
```

- Install Alarmy Modified ( Replaces Google Deskclock )

```bash
TARGET_REPLACE_GOOGLE_CLOCK := true
```


## Advanced usage

### Initial setup

- Install `xmlstarlet`

  - `sudo apt install xmlstarlet`, `sudo pacman -S xmlstarlet`, etc. depending on your distribution.
  
- Install `apktool`, make sure its available in PATH

  - https://ibotpeaches.github.io/Apktool/install/
  - Run `apktool if framework-res.apk` to install framework for apktool this is necessary for proper extracting
    (generally found in /system/framework/)
  - Run `apktool if framework-res__auto_generated_rro_product.apk` to install framework overlay for apktool this is necessary for proper extracting
    (generally found in /product/overlay/)

### Extracting / updating extracted overlays

Basically the same as regular extract-files

```sh
./extract-files.sh /path/to/a/dump
```

### Removing certain overlays

- If certain overlays are not required, you can simply add them to `exclude-tag.txt`, those will be removed from the extracted overlays.

### Miscellaneous information

- The main changes here are the addition of `apktool` and `xmlstarlet`, to allow us to extract the necessary overlays from some RROs.
- Since the process does take a while, to speed it up, all extraction is done in the background. This might not be suitable for weaker systems if there is a very high number of overlays to be extracted.
